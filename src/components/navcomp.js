import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import TextField from '@material-ui/core/TextField';
import facebookicon from '../pages/facebookicon.png';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


class NavButton extends React.Component{
state = {
    anchorEl: null,
  };
    render(){
    	return(

    	  <div>
	       <div class="row" id="header">
            <p class="three columns" id="hours">
              Monday: CLOSED <br />
              Tuesday-Friday: 10:00am - 7:00pm<br />
              Saturday: 9:00am - 2:00pm <br />
              Sunday: CLOSED <br />
            </p>

            <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id="logo"/>

            <p class="three columns" id="address">
              6750 4th Ave, Brooklyn, New York, 11220
              <div>
                <a href="https://www.facebook.com/LovingPawsAnimalHospital/"><img id="imageOne" src={facebookicon}></img></a>
              </div>
            </p>


          </div>



          <div class="row" id="navbar">

            <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>
            <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
            <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
	            <Menu
			          anchorEl={this.state.anchorEl}
			          open={this.state.open}
			          onRequestClose={this.handleRequestClose}>
			          <MenuItem onClick={this.handleRequestClose}>Profile</MenuItem>
			          <MenuItem onClick={this.handleRequestClose}>My account</MenuItem>
			          <MenuItem onClick={this.handleRequestClose}>Logout</MenuItem>
	       			 </Menu>
            <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
            <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
            <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>





          </div>
    	</div>
    	)
    }
};

export default NavButton
