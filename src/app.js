/*
   --------
   import the packages we need
   --------
 */

import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './style/theme';
import initialState from './initialState.json';
import './style/main.css';




/*
   --------
   import your pages here
   --------
 */

import HomePage from './pages/home';
import ResourcesPage from './pages/resources';
import AboutPage from './pages/about';
import ServicesPage from './pages/services';
import FAQPage from './pages/faq';
import ContactPage from './pages/contact';
import LoginPage from './pages/login';
import SignupPage from './pages/signup';




/*
   --------
   configure everything
   --------
 */

const firebaseConfig = {
    /*
       --------
       REPLACE WITH YOUR FIREBASE CREDENTIALS
       --------
     */
    apiKey: "AIzaSyBvUEriWYaTTrJilT9WJckFFVMyL3xY5bE",
    authDomain: "the-stuff.firebaseapp.com",
    databaseURL: "https://the-stuff.firebaseio.com",
    projectId: "the-stuff",
    storageBucket: "the-stuff.appspot.com",
    messagingSenderId: "707524126013"

};

// react-redux-firebase config
const rrfConfig = {
  userProfile: 'users',
};

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);









/*
   --------
   setup redux and router
   --------
 */


const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig)
)(createStore);

// Add firebase to reducers
const rootReducer = combineReducers({
  firebase: firebaseReducer
});

const store = createStoreWithFirebase(rootReducer, initialState);


const ConnectedRouter = connect()(Router);



export default class App extends React.Component{
  render(){
	  return(
	    <MuiThemeProvider theme={theme}>
		    <Provider store={store}>
		      <ConnectedRouter>
      			<div>
      			  <Route exact path="/" component={HomePage} />
      			  <Route exact path="/resources" component={ResourcesPage} />
      			  <Route exact path="/about" component={AboutPage} />
              <Route exact path="/services" component={ServicesPage} />
              <Route exact path="/faq" component={FAQPage} />
              <Route exact path="/contact" component={ContactPage} />
              <Route exact path="/login" component={LoginPage} />
              <Route exact path="/signup" component={SignupPage} />
            </div>
		      </ConnectedRouter>
		    </Provider>
	    </MuiThemeProvider>
	  );
  }
}
