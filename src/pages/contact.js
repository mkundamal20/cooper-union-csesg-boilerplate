import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import NavButton from '../components/navcomp.js';
import TextField from '@material-ui/core/TextField';


class ContactPage extends React.Component{
  render(){

    return(
      <div>
          <div class="row" id="header">
            <p class="three columns" id="hours">
              Monday: CLOSED <br />
              Tuesday-Friday: 10:00am - 7:00pm<br />
              Saturday: 9:00am - 2:00pm <br />
              Sunday: CLOSED <br />
            </p>

            <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id="logo"/>

            <p class="three columns" id="address">
              6750 4th Ave, Brooklyn, New York, 11220
            </p>

          </div>

          <div class="row" id="navbar">
          <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>
          <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
          <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
          <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
          <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
          <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>
          </div>

          <br />
          <br />

          <div id="headerAbout">
            Contact Us
          </div>

          <br />
          <br />

    <form>
       <div class="row">
         <div class="six columns">
           <label for="exampleEmailInput">Your email</label>
           <input class="u-full-width" id="exampleEmailInput" type="email" placeholder="test@mailbox.com" />
         </div>
         <div class="six columns">
           <label for="exampleRecipientInput">Reason for contacting</label>
           <select class="u-full-width" id="exampleRecipientInput">
             <option value="Option 1">Questions</option>
             <option value="Option 2">Admiration</option>
             <option value="Option 3">Medical Concern</option>
           </select>
         </div>
       </div>
       <label for="exampleMessage">Message</label>
       <textarea class="u-full-width" id="exampleMessage" placeholder="Enter your message here!"></textarea>
       <label class="example-send-yourself-copy">
         <input type="checkbox" />
         <span class="label-body">Send a copy to yourself</span>
       </label>
      <input class="button-primary" type="submit" value="Submit" />
    </form>
    </div>

    )


	  return(
	    <div>
        <div class="row" id="header">
          <p class="three columns" id="hours">
            Monday: CLOSED <br />
            Tuesday-Friday: 10:00am - 7:00pm<br />
            Saturday: 9:00am - 2:00pm <br />
            Sunday: CLOSED <br />
          </p>


          <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id = "logo"/>

          <p class="three columns" id="address">
            6750 4th Ave, Brooklyn, New York, 11220
          </p>

        </div>

        <div class="row" id="navbar">
          <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>
          <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
          <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
          <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
          <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
          <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>
        </div>

        <br />
        <br />

        <div id="headerAbout">
          Contact Us
        </div>

        <TextField>
          id="uncontrolled"
          label="Name"
          defaultValue=""
          className={ContactPage.textField}
          margin="normal"
        </TextField>

        <br />
        <br />

        <TextField>
          id="uncontrolled"
          label="Pet"
          defaultValue=""
          className={ContactPage.textField}
          margin="normal"
        </TextField>

        <br />
        <br />

        <TextField
          id="uncontrolled"
          label="Email"
          defaultValue=""
          className={ContactPage.textField}
          margin="normal"
        />

        <TextField
          id="uncontrolled"
          label="Phone #"
          defaultValue=""
          className={ContactPage.textField}
          margin="normal"
        />

        <br />
        <br />

        <TextField
          id="uncontrolled"
          label="Message"
          defaultValue=""
          className={ContactPage.textField}
          margin="normal"
        />
        </div>)
};
}


export default ContactPage
