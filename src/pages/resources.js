import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import NavButton from '../components/navcomp.js';

class ResourcesPage extends React.Component{
  render(){
    return(
	    <div>
        <div class="row" id="header">
          <p class="three columns" id="hours">
            Monday: CLOSED <br />
            Tuesday-Friday: 10:00am - 7:00pm<br />
            Saturday: 9:00am - 2:00pm <br />
            Sunday: CLOSED <br />
          </p>

          <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id = "logo"/>

          <p class="three columns" id="address">
            6750 4th Ave, Brooklyn, New York, 11220
          </p>

        </div>

        <div class="row" id="navbar">
          <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>
          <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
          <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
          <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
          <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
          <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>
        </div>

        <br />
        <br />

        <div id="headerAbout">
          Resources
        </div>
        <br />
    		<Link to="/">Adoption</Link><br />
        <Link to="/">Hospital Forms</Link><br />
        <Link to="/">Pet Insurance</Link><br />
      </div>
	  )
  }
};

export default ResourcesPage
