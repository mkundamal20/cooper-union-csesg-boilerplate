import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import NavButton from '../components/navcomp.js';

class AboutPage extends React.Component{
  render(){
    return(
  	  <div>
          <div class="row" id="header">
            <p class="three columns" id="hours">
              Monday: CLOSED <br />
              Tuesday-Friday: 10:00am - 7:00pm<br />
              Saturday: 9:00am - 2:00pm <br />
              Sunday: CLOSED <br />
            </p>

            <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id="logo"/>

            <p class="three columns" id="address">
              6750 4th Ave, Brooklyn, New York, 11220
            </p>

          </div>

            <div class="row" id="navbar">
            <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>

            <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
            <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
            <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
            <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
            <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>
          </div>

          <br />
          <br />
         

          <div id="headerAbout">
            About Us
          </div>
             <br />
             <br />
          <h2 class="headerAbout2">
            How We Started
          </h2>
                    <h3> Welcome to Loving Paws Animal Hospital! We are an animal hospital in Brooklyn dedicated to helping your pets. We cannot wait to meet you! </h3>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          <h2 class = "headerAbout2">
            Our Mission
          </h2>
                    <h3> Here at Loving Paws, we are committed to excellence, compassion, and making a difference! We love your pets, and know you love them too! Our goal is to help your family be as healthy and happy as possible. </h3>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          <h2 class= "headerAbout2">
            Meet Our Team!
          </h2>
                    <h3>Our staff is comprised of doctors and volunteers passionate about helping all animals!</h3>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
      </div>
	  )
  }
};

export default AboutPage
