import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import NavButton from '../components/navcomp.js';

class Maps extends React.Component{
  render(){
    return(
  	  <div>
        <div class="row" id="header">
            <p class="three columns" id="hours">
              Monday: CLOSED <br />
              Tuesday-Friday: 10:00am - 7:00pm<br />
              Saturday: 9:00am - 2:00pm <br />
              Sunday: CLOSED <br />
            </p>

            <img src = {require('../resources/LovingPawsLogoHeader.png')} alt="Loving Paws Logo" class="six columns" id="logo"/>

            <p class="three columns" id="address">
              6750 4th Ave, Brooklyn, New York, 11220
            </p>
        </div>

        <div class="row" id="navbar">
            <Link to="/" style={{ color: '#FFF' }}><button>Home</button></Link>
            <Link to="/about" style={{ color: '#FFF' }}><button>About Us</button></Link>
            <Link to="/services" style={{ color: '#FFF' }}><button>Services</button></Link>
            <Link to="/resources" style={{ color: '#FFF'}}><button>Resources</button></Link>
            <Link to="/contact" style={{ color: '#FFF' }}><button>Contact Us</button></Link>
            <Link to="/faq" style={{ color: '#FFF' }}><button>FAQ</button></Link>
        </div>

        <br />
        <br />
        <br />
        <br />

        <body>
          <h3>My Google Maps Demo</h3>
          <!--The div element for the map -->
          <div id="map"></div>

          <script>
            // Initialize and add the map
            function initMap() {
              // The location of Uluru
              var uluru = {lat: -25.344, lng: 131.036};
              // The map, centered at Uluru
              var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 4, center: uluru});
              // The marker, positioned at Uluru
              var marker = new google.maps.Marker({position: uluru, map: map});
            }
          </script>
          <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj4kKPS99A--9ZyubpwW4atTs6h7VXO8E&callback=initMap" type="text/javascript">
          </script>
        </body>
      </div>
	  )
  }
};

export default Maps
